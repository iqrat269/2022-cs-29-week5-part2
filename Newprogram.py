
# Basic libraries to import for completing the whole work.
import sys
import PyQt5
from PyQt5.uic import loadUi
# from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd



class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        loadUi("app.ui", self)  # Here we imported the QT Designer file which we made as Python GUI FIle.

        # Command to remove the default Windows Frame Design.
        self.setWindowFlag ( QtCore.Qt.FramelessWindowHint )
        # Command to make the backgroud of Window transparent.
        self.setAttribute ( QtCore.Qt.WA_TranslucentBackground )

        # These 2 lines are used to put funnctions on close and minimize buttons.
        self.MinimizeButton.clicked.connect ( lambda : self.showMinimized () )
        self.CrossButton.clicked.connect ( lambda : self.close () )


        # Button Functions
        self.SaveButton.clicked.connect ( self.Add_student )
        self.ResetButton.clicked.connect ( self.resetValues )




    # This is a helping function to put the text fields back after adding or editing a STudent.
    def resetValues(self) :
        self.District.setText ( "Lahore" )
        self.tehsil.setText ( "Select Tehsil" )
        self.StampLine.setText ( "select stamp paper type" )
        self.DeedNameLine.setText ( "Select Deed Name" )
        self.AgentNameline.setText.clear()
        self.AgentCLine.setText.clear ()
        self.AgentContctLine.setText.clear ()
        self.AgentEmail.setText.clear ()



    # This Function will take some values from the textfields and will add the data of student to the
    # CSV FIle if the registration number is Written is not added before.
    def Add_student(self) :
        district = self.District.text ()
        tehsil = self.tehsil.text ()
        stamp =  self.StampLine.text ()
        deedName = self.DeedNameLine.text ()
        AgentName=self.AgentNameline.text()
        agentCNIC=self.AgentCLine.text()
        agentCont=self.AgentContctLine.text()
        agentEmail=self.AgentEmail.text()

        if (tehsil != "Select Tehsil") :

            student_data = [district , tehsil , stamp , deedName , AgentName,agentCNIC,agentCont,agentEmail]

            with open ( 'student.csv' , 'a+' , encoding = "utf-8" , newline = "" ) as fileInput :
                writer = csv.writer ( fileInput )
                writer.writerows ( [student_data] )
            self.resetValues ()
        else :
            msg = QMessageBox ()
            msg.setWindowTitle ( "--- Add Details of Challan ---" )
            msg.setText ( "Registration Number is already Added." )
            msg.setIcon ( QMessageBox.Information )
            msg.setStandardButtons ( QMessageBox.Ok | QMessageBox.Cancel )
            font = QtGui.QFont ()
            font.setPointSize ( 12 )
            font.setBold ( True )
            font.setWeight ( 75 )
            msg.setFont ( font )
            msg.exec ()

        # This is to reload the table so that recent added data shows in the table







# main
app = QApplication(sys.argv)
window = MainWindow()
window.show()
sys.exit(app.exec_())



